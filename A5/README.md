# LIS3781 - Advanced Database Management

## Juan Ascanio

### Assignment 5 Requirements:

*Two Parts:*

1. Screenshot of ERD
2. SQL Code *Optional*

#### README.md file should include the following items:

* Screenshot of project ERD
* Screenshots of *Optional* SQL Code

#### Assignment 5 Screenshots:

*Screenshot of project ERD*:
![A5 ERD](img/A5_ERD.png)

*Screenshots of A5 1*:
![A5 1](img/A5_1.png)

*Screenshots of A5 2*:
![A5 2](img/A5_2.png)

*Screenshots of A5 3*:
![A5 3](img/A5_3.png)

*Screenshots of A5 4*:
![A5 4](img/A5_4.png)

*Screenshots of A5 5*:
![A5 5](img/A5_5.png)

*Screenshots of A5 6*:
![A5 6](img/A5_6.png)