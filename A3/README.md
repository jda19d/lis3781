# LIS3781 - Advanced Database Management

## Juan Ascanio

### Assignment 3 Requirements:

*Two Parts:*

1. SQL Code
2. Populated Tables
3. SQL Code

#### README.md file should include the following items:

* Screenshot of my SQL Code
* Screenshot of populated tables

#### Assignment Screenshots:


*Screenshot of Creation and Insert SQL Queries 1*:

![SQL Code 1](img/a3_1.png)

*Screenshot of Creation and Insert SQL Queries 2*:
![SQL Code 2](img/a3_2.png)

*Screenshot of Creation and Insert SQL Queries 3*:
![SQL Code 3](img/a3_7.png)


*Screenshot of Populated Tables 1*:

![Populated Tables 1](img/a3_3.png)

*Screenshot of Populated Tables 2*:

![Populated Tables 2](img/a3_6.png)

*Screenshot of Populated Tables 3*:

![Populated Tables 3](img/a3_5.png)

*Screenshot of User Version*:

![User Version](img/a3_10.png)

*Screenshot of customer number, last name, first name, and e-mail*:

![List 1](img/a3_4.png)

*Screenshot of customer number, last name, first name, and address of every customer who lives on a street with "East" in the name*:

![List 2](img/a3_8.png)

*Screenshot of aggregate values for customers*:

![List 3](img/a3_9.png)
