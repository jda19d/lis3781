> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS3781 - Advanced Database Management

## Juan Ascanio

### LIS3781 Requirements:

*Course Work Links:*

1. [A1 README.md](A1/README.md "My A1 README.md file")
    - Install AMPPS
    - Provide screenshots of installations
	- Create Bitbucket repo
	- Complete Bitbucket tutorial (bitbucketstationlocations)
	- Provide git command descriptions

2. [A2 README.md](A2/README.md "My A2 README.md file")
	- Use SQL to create tables
	- Use SQL to hash tables to protect valuable data
	- Use SQL to grant permissions

3. [A3 README.md](A3/README.md "My A3 README.md file")
	- Use Oracle SQL Server to create and populate tables.
	- Use queries to display different reports.
	- Use queries to change and siplay differences in tables.
	- Use queries to list tables in certain orders.
4. [P1 README.md](P1/README.md "My P1 README.md file")
	- Use MySQL to create a database for a local law firm
	- Use hashing to hide SSN
	- Create an ERD to display data
	- Use SQL statements to retrieve different types of data
5. [A4 README.md](A4/README.md "My A5 README.md file")
	- Use MS SQL Server to create a database
	- Use hashing to hide SSN
	- Create triggers

6. [A5 README.md](A5/README.md "My A5 README.md file")
	- Use MS SQL Server to data warehouse an existing database
	- Add more tables and data to the database
	- Use SQL Statments to create triggers

7. [P2 README.MD](P2/README.MD "My P2 README.md file")
	- Use MongoDB and import a JSON file
	- Use NoSQL to show reports
	- Use MongoDB Shell commands to show collections






