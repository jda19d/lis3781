> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Juan Ascanio

### Assignment 1 Requirements:

*Five Parts:*

1. Distributed Version Control with Git and Bitbucket
2. AMPPS Installation
3. Questions
4. Entity Relationship Diagram, and SQL Code
5. Bitbucket repo links:
   a)this assignment and
   b)the completed tutorial (bitbucketstationlocations).

#### README.md file should include the following items:

* Screenshot of ampps installation running
* git commands w/short descriptions
* ERD image
* Bitbucket repo links of this assignment and bitbucketstationlocations

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - creates new git repository.
2. git status - displays state of the working directory and staging area.
3. git add - adds a change in the working directory to the staging area.
4. git commit - captures a snapshot of the project's currently staged changes. 
5. git push - upload local repository content to a remote repository. 
6. git pull - update local repository with files from a remote repository.
7. git branch - list all branches in your repo.

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.png)


*Screenshot of A1 ERD*:

![A1 ERD Screenshot](img/erd_A1.png)

*Screenshot of A1 Ex1*:

![A1 Ex1 Screenshot](img/ex1_A1.png)

*Screenshot of A1 Ex2*:

![A1 Ex2 Screenshot](img/ex2_A1.png)

*Screenshot of A1 Ex3*:

![A1 Ex3 Screenshot](img/ex3_A1.png)

*Screenshot of A1 Ex4*:

![A1 Ex4 Screenshot](img/ex4_A1.png)

*Screenshot of A1 Ex5*:

![A1 Ex5 Screenshot](img/ex5_A1.png)

*Screenshot of A1 Ex6*:

![A1 Ex6 Screenshot](img/ex6_A1.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/jda19d/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")
