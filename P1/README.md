# LIS3781 - Advanced Database Management

## Juan Ascanio

### Project 1 Requirements:

*Two Parts:*

1. Screenshot of ERD
2. SQL Code

#### README.md file should include the following items:

* Screenshot of project ERD
* Screenshot of SQL code for required reports

#### Assignment Screenshots:

*Screenshot of project ERD*:
![SQL Code 1](img/ERD_snip.png)

*Screenshot of Creation and Insert SQL Queries 2*:
![SQL Code 2](img/P1_#1.png)

*Screenshot of  Person Table*:
![SQL Code 3](img/person_table.png)

