# LIS3781 - Advanced Database Management

## Juan Ascanio

### Assignment 4 Requirements:

*Two Parts:*

1. Screenshot of ERD
2. SQL Code *Optional*

#### README.md file should include the following items:

* Screenshot of project ERD
* Screenshots of *Optional* SQL Code

#### Assignment Screenshots:

*Screenshot of project ERD*:
![A4 ERD](img/a4_erd.PNG)

*Screenshots of A4 1*:
![A4 1](img/A4_1.png)

*Screenshots of A4 2*:
![A4 2](img/a4_2.png)

*Screenshots of A4 3*:
![A4 3](img/A4_3.png)

*Screenshots of A4 4*:
![A4 4](img/A4_4.png)

*Screenshots of A4 5*:
![A4 5](img/A5_5.png)

